class CancelablePromise extends Promise {
  static createdPromisesCount = 0

  constructor(executor, name) {
    if (
      typeof executor !== 'function' &&
      executor !== null &&
      executor !== undefined
    )
      throw new TypeError('Executor must be a function or null')

    CancelablePromise.createdPromisesCount++

    let isCanceled = false

    super((resolve, reject) => {
      executor(
        value => {
          if (isCanceled) reject({ isCanceled: true })
          else resolve(value)
        },
        reason => {
          if (isCanceled) reject({ isCanceled: true })
          else reject(reason)
        }
      )
    })

    this.name = name ? name : 'system'
    this.id = CancelablePromise.createdPromisesCount
    this.parent = null
    this.childs = []

    Object.defineProperty(this, 'isCanceled', {
      get: function () {
        return isCanceled
      },
      set: function (val) {
        isCanceled = val
      },
      enumerable: true,
    })
  }

  then(onFulfilled, onRejected, name) {
    if (
      typeof onFulfilled !== 'function' &&
      onFulfilled !== null &&
      onFulfilled !== undefined
    ) {
      throw new TypeError('onFulfilled argument must be a function or null')
    }
    if (
      typeof onRejected !== 'function' &&
      onRejected !== null &&
      onRejected !== undefined
    ) {
      throw new TypeError('onRejected argument must be a function or null')
    }

    const childPromise = new CancelablePromise((resolve, reject) => {
      super.then(
        result => {
          if (typeof onFulfilled === 'function') {
            try {
              resolve(onFulfilled(result))
            } catch (error) {
              reject(error)
            }
          } else {
            resolve(result)
          }
        },
        error => {
          if (typeof onRejected === 'function') {
            try {
              resolve(onRejected(error))
            } catch (error) {
              reject(error)
            }
          } else {
            reject(error)
          }
        }
      )
    }, name)

    childPromise.parent = this

    this.childs.push(childPromise)

    return childPromise
  }

  cancel() {
    let topParent = this

    while (topParent.parent) {
      topParent = topParent.parent
    }

    topParent.isCanceled = true

    const cancelChildrens = children => {
      for (const child of children) {
        if (child.name !== 'system') child.isCanceled = true
        if (child.childs && child.childs.length > 0)
          cancelChildrens(child.childs)
      }
    }
    
    cancelChildrens(topParent.childs)
  }
}

module.exports = CancelablePromise
